@extends('master')
@section('content')
<div class="card">
    <div class="card-body">
        <div class="d-flex justify-content-between align-items-baseline mb-2">
            <h6 class="card-title">Data Produk</h6>
            <button type="button" class="btn btn-primary btn-icon-text">
                <i class="btn-icon-prepend" data-feather="plus"></i>
                Add Data
            </button>
        </div>


        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama Product</th>
                        <th>Sale</th>
                        <th>Price</th>
                        <th>Image</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>1</th>
                        <td>Product Kita</td>
                        <td><span class="btn btn-danger btn-xs">Discount 30%</span></td>
                        <td>Rp. 355.000</td>
                        <td><img src="https://dbs9nopbkp043.cloudfront.net/images/products/CKL-723304_1655886797661_resized1024.jpg"
                                class="img-fluid rounded" alt=""></td>
                        <td><a href="#" class="btn btn-success btn-xs">Aktif</a></td>
                        <td>
                            <a href="#" class="btn btn-primary btn-icon me-2">
                                <i data-feather="edit"></i>
                                </button>
                                <a href="#" class="btn btn-danger btn-icon ">
                                    <i data-feather="trash-2"></i>
                                    </button>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection